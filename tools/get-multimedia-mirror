#!/bin/bash

###############################################################################
#    (C) 2009-2011 hyperclock (aka Jimmy M. Coleman)
#	 website:  www.cybernux.org
#	 email:    hyperclock(at)cybernux(dot)org
#	 git:	   http://vcs.tektystiks.com/the-cybernux-linux-project/buildforge.git
# 
###############################################################################
###############################################################################
#    BuildForge - Scripts designed to build Cybernux Linux® based on Debian.
#
#    BuildForge is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    BuildForge is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy (see docs/gpl-3.0.txt) of the GNU General 
#    Public License along with BuildForge.  If not, see 
#    <http://www.gnu.org/licenses/>.
#
############################################################################


# ---------------------------------------
# Simple debmirror update script example
# Make your changes below
# --------------------------------------

ARCHITECTURES="i386,amd64"
SECTIONS="main,non-free"
DISTS="stretch"
#DISTS="jessie"
REMOTE_MIRROR="www.deb-multimedia.org"
#REMOTE_MIRROR="192.168.2.104/debian-mm"
LOCAL_MIRROR="/deb-mirror/debian-mm"


# ----------------------------------------------------------------------
# put OPT_SOURCES as "--source" or leave blank to have sources included
# ----------------------------------------------------------------------
OPT_SOURCES="--source"


# -------------------------------------------------------
# Don't change below, unless you know what you are doing.
# -------------------------------------------------------
LOCK="${LOCAL_MIRROR}/mirror.lock"


if [ -f "${LOCK}" ]; then
        echo "ERUNNING: \"`basename ${0}`\" is already running."
        exit 1
else
        trap "test -f ${LOCK} && rm -f ${LOCK}; exit 0" 0 2 15

        touch ${LOCK}

        debmirror --arch="${ARCHITECTURES}" --section="${SECTIONS}" \
                --method=http --root="/" ${OPT_SOURCES} \
                --host="${REMOTE_MIRROR}" --md5sums \
                --dist="${DISTS}" --diff=none --i18n --omit-suite-symlinks \
                --no-check-gpg \
                --ignore-missing-release \
                --ignore-release-gpg \
                --ignore-small-errors \
                --progress "${LOCAL_MIRROR}"


fi

# Extract the lists so awk can handle them
find $LOCAL_MIRROR/dists/ -name Packages.bz2 -exec bunzip2 -fk {} \;
find $LOCAL_MIRROR/dists/ -name Sources.bz2 -exec bunzip2 -fk {} \;

