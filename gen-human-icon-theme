#!/bin/bash

###############################################################################
#    (C) 2013-2017 hyperclock (aka Jimmy M. Coleman)
#	 website:  www.hubshark.com
#	 email:    hyperclock(at)hubshark(dot)com
###############################################################################
###############################################################################
#    BuildForgeHOS - Scripts designed to build HubShark OS based on Debian GNU/Linux.
#
#    BuildForgeHOS is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    BuildForgeHOS is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy (see COPYING) of the GNU General 
#    Public License along with BuildForgeHOS.  If not, see 
#    <http://www.gnu.org/licenses/>.
#
################################################################################
################################ INFO ##########################################
#
#	Derived from the scripts used by the gNewSense Project.
#	See the original scripts at http://gnewsense.org
#
#	Original (C) 2006 - 2009  Brian Brazil
#
#################################################################################


. config

IMGDIR=$PWD/images
rm -rf $WORKINGDIR
mkdir -p $WORKINGDIR
cd $WORKINGDIR

apt-get source human-icon-theme$VERSION
apt-get --yes build-dep human-icon-theme$VERSION
cd human-icon-theme-*

# CC-BY-SA doesn't belong in non-free.
sed -i -e "s#Section: non-free/x11#Section: x11#g" debian/control.in

# Icons
if [ -f $IMGDIR/$DISTRONAME_L-icon-48x48.png ]; then
	cp $IMGDIR/$DISTRONAME_L-icon-48x48.png 48x48/places/start-here.png
else
	convert -size 48x48 xc:transparent -fill $LOGO_COLOUR -gravity Center -pointsize 44 -draw "text 0,-4 '$LOGO_LETTER'" 48x48/places/start-here.png
fi

if [ -f $IMGDIR/$DISTRONAME_L-icon-24x24.png ]; then
	cp $IMGDIR/$DISTRONAME_L-icon-24x24.png 24x24/places/start-here.png
else
	convert -size 24x24 xc:transparent -fill $LOGO_COLOUR -gravity Center -pointsize 20 -draw "text 0,-2 '$LOGO_LETTER'" 24x24/places/start-here.png
fi

if [ -f $IMGDIR/$DISTRONAME_L-icon-22x22.png ]; then
	cp $IMGDIR/$DISTRONAME_L-icon-22x22.png 22x22/places/start-here.png
else
	convert -size 22x22 xc:transparent -fill $LOGO_COLOUR -gravity Center -pointsize 18 -draw "text 0,-2 '$LOGO_LETTER'" 22x22/places/start-here.png
fi

if [ -f $IMGDIR/svg/$DISTRONAME_L.svg ]; then
	cp $IMGDIR/svg/$DISTRONAME_L.svg scalable/places/start-here.svg
else
	convert -size 48x48 xc:transparent -fill $LOGO_COLOUR -gravity Center -pointsize 44 -draw "text 0,-4 '$LOGO_LETTER'" ./scalable/places/start-here.svg
fi

echo | dch -D $RELEASE -v $(sed  -n '1s#^.*(\(.*\)).*#\1'${DISTRONAME_L}${HUMAN_ICON_THEME_VERSION}'#p' debian/changelog)  "Munged to show $DISTRONAME graphics"

dpkg-buildpackage $DPKGOPTS
